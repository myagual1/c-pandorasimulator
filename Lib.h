#ifndef LIB_H
#define LIB_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <math.h>    
#include <cstring>

#include "Song.h"
#include "Time.h"


using namespace std;

class Lib{
	public:
		Lib();
		void run(string filename);
		void init(string t, int max);
		void add(string title, string artist, string length); 
		void quit();
		void play(int n);
		void rest(string r);		
		void like();
		void dislike();
		void like(string title);
		void dislike(string title);

		void print_playlist();
		//hash functions
		int Hash(string key);
		void insert_hashtable(Song* sn);
		Song* search(string key);

	private:
		Song* playlist;
		int maxSongs; 
		int currSongs; 
		Time* currTime;
		Song song_currently_playing;
		Song** hashtable;		

		//heap functions
		void update_priorities();
		int heap_key(Song& s);
		int getleft(int parent);
    int getright(int parent);
    int getparent(int child);

		bool comp(int pnt,int pos);

		void swap(int i, int j);
		void buildmaxheap();
		void maxheapify(int i);
};

#endif	

