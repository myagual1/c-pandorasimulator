#ifndef SONG_H
#define SONG_H


#include<iostream>
#include <string>
#include "Time.h"

using namespace std;

class Song{
	public:
		//void convert_to_time(string runtime);
	Song();
	Song(string ti, string ar, Time* len);			
	void like();
	void dislike();
	void printSong();	
	void update_key(Time* currtime, Time* lastplayedsecs);

	int likes;
	string title;
	string artist;
	Time* runtime;
	Time* lastplayed; 
	Time* timeadded;
	int key;
};

#endif
