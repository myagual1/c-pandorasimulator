#include "Time.h"
#include "Song.h"
#include "Lib.h"

using namespace std;

int main(int argc, char* argv[]){
	Lib newLib;	
	if(argc == 2){
		newLib.run(argv[1]); 
	}
	else{
		cout << "If you want to read from a file add it as an argument after ./Yagual_Mariuxi_p2" <<endl;
		cout << "or you can write the filename down here! or type NO to insert commands individually" <<endl;
		string file;		
		cin >> file;
		if(file.compare("NO") != 0){
			newLib.run(file); 
		}
		else{
			string line, word;
			cout << "Enter a command INIT" << endl ;
  		cout << "->";
			cin.ignore();
 			getline(cin, line);
			stringstream s0;
			s0 <<line;
			s0 >>word;
			if(word != "INIT"){
				cout<<"You need the INIT command first for the player to work propertly, try again" <<endl;
				exit(1);
			}
			string tmp1;
			int tmp2;
			s0 >>tmp1;
			s0 >>tmp2;
			newLib.init(tmp1, tmp2);
			
			while(word != "QUIT"){
				cout << "Enter a command" << endl ;
   	 		cout << "->";
    		getline(cin, line);
				stringstream ss;
    		ss << line;
  			ss >> word;
				if(word.compare("ADD") == 0){
					string p;
					string d;
					string word;
					getline(ss, p, ',');
					getline(ss, d, ',' );	
					ss >> word;
					newLib.add(p, d, word);
    		}	
    		else if(word.compare("PLAY") == 0){
      		int tmp1 =0;
					ss >> tmp1;
					newLib.play(tmp1);
    		}
    		else if(word.compare("REST") == 0){
      		string tmp1;
					ss >>tmp1;
					newLib.rest(tmp1);
    		}
    		else if(word.compare("LIKE") == 0){ 
      		string tmp1;
					if(getline(ss, tmp1)){
					newLib.like(tmp1);
					}
					else newLib.like();
    		}
    		else if(word.compare("DISLIKE") == 0){
      		string tmp1; 
        	if(getline(ss, tmp1)){
     				newLib.dislike(tmp1);                                         				}	
					else	newLib.dislike();
    		}
				cout << endl;
			}
		}
	}
	return 0;
}
