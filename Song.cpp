#include "Song.h"

Song::Song(){
	title = "";
	artist = "";
	runtime = NULL;
	timeadded = NULL;
	lastplayed = NULL;
	likes = 0;
	key = 0;
}

Song::Song(string ti, string ar, Time* len){
	title = ti;
	artist = ar;
	runtime = len;
	likes = 0;
}

void Song::like(){
	if(likes >= 0){
		likes++;
	}
	else{
		likes = 1;
	}
}

void Song::dislike(){
	if(likes <= 0){
		likes--;
	}
	else{
		likes = -1;
	}
}

void Song::printSong(){
	cout << title << " by " << artist << endl;
}

void Song::update_key(Time* currtime, Time* lastplayedsecs){
	int t2 = currtime->convert_to_seconds();
	int t3 = lastplayedsecs->convert_to_seconds();
	int sub = t2 - t3; 
	key = sub + (100*(likes));
//	cout << t2 << "-" << t3 << "=" << sub << " key: " << key  <<endl;
	return;
}


