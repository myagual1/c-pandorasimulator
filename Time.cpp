#include "Time.h"

Time::Time(){
	day = 0;
	hour = 0;
	min = 0;
	sec = 0;
}

Time::Time(int d, int h, int m, int s){                           
  day = d;                              
  hour = h;                             
  min = m;                              
  sec = s;                              
}

Time::Time(int h, int m, int s){
	day = 1;
	hour = h;
	min = m;
	sec = s;
	adjust_time();
}

Time::Time(int m, int s){
	day = 0;
	hour = 0;	
	min = m;
	sec = s;
	adjust_time();
}

Time::~Time(){

}

void Time::add_time(Time* t2){
	day += (t2->day);
	hour += (t2->hour);
	min += (t2->min);
	sec += (t2->sec);
	adjust_time();
}

void Time::adjust_time(){
	while(sec >= 60){
		sec -= 60;
		min++;
	}
	while(min >= 60){
		min -= 60;
		hour++;
	}
	while(hour >= 24){
		hour -= 24;
		day++;
	}
}

void Time::printTimestamp(){
	int nh = 0;
	if(hour > 12){
	 	nh = hour - 12;
		cout << "[" << "Day " << day << ": " << nh <<":"<<min<<":"<<sec <<"pm"<< "] ";
	}
	else if(hour == 12){
    cout << "[" << "Day " << day << ": " << hour <<":"<<min<<":"<<sec <<"pm"<< "] ";
	}
	else{
		cout << "[" << "Day " << day << ": " << hour <<":"<<min<<":"<<sec <<"am" <<"] ";
	}
}

int Time::convert_to_seconds(){
	int dayinsec = (day * 86400);
	int hourinsec = (hour*3600); 
	int mininsec = (min*60); 
	int sum = dayinsec + hourinsec+ mininsec+ sec;
	return sum;
}


