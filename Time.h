#ifndef TIME_H
#define TIME_H

#include<iostream>
#include<string>
#include<cstdlib>

using namespace std;

class Time{
	public:
	Time();
	Time(int d, int h, int m, int s); 
	Time(int h, int m, int s);
	Time(int m, int s);
		
	void add_time(Time* t2);
	void printTimestamp();
	~Time();

	int day;
	int hour;
	int min;
	int sec;
	
	void adjust_time(); //check if min is more than 60 then it will change hour
	
	int convert_to_seconds();
};

#endif
