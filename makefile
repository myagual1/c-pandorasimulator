FLAGS = -Wall -g -std=c++11

all:	p2

p2:	Lib.o Time.o Song.o test.o
	g++ $(FLAGS) Lib.o Time.o Song.o test.o -o Yagual_Mariuxi_p2

List.o :	Lib.cpp
	g++ $(FLAGS) -c List.cpp  

Time.o :	Time.cpp
	g++ $(FLAGS) -c Time.cpp    

Song.o :	Song.cpp
	g++ $(FLAGS) -c Song.cpp  

test.o :	test.cpp
	g++ $(FLAGS) -c test.cpp 

clean :
	-rm -f *.o	Yagual_Mariuxi_p2
