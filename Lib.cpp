#include "Lib.h"

Lib::Lib(){
	maxSongs = 0;
	currSongs = 0;
	song_currently_playing = Song();
	currTime = NULL;
}

void Lib::run(string filename){
	ifstream filep(filename.c_str());
	string line, word;	
	getline(filep, line);
	stringstream s0;
	s0 <<line;
	s0 >>word;
	string tmp1;
	int tmp2;
	s0 >>tmp1;
	s0 >>tmp2;
	init(tmp1, tmp2);
	while(getline(filep, line)){
			stringstream ss;
			ss << line;
  		ss >> word;
			if(word.compare("ADD") == 0){
				string p;
				string d;
				string word;
				getline(ss, p, ',');
				getline(ss, d, ',' );	
				ss >> word;
			add(p, d, word);
			}
			if(word.compare("PLAY") == 0){
				int tmp1;
				ss >> tmp1;
				play(tmp1);
			}
			if(word.compare("LIKE") == 0){
				string tmp1;
				if(getline(ss, tmp1)){
					like(tmp1);
				}
				else like();
			}
			if(word.compare("DISLIKE") == 0){                           
        string tmp1;                                           
        if(getline(ss, tmp1)){                                        
          dislike(tmp1);                                          
        }                                                      
        else dislike();                                           
      }	
			if(word.compare("REST") == 0){
				string tmp1;
				ss >>tmp1;
				rest(tmp1);
			}
			if(word.compare("QUIT") == 0){
				return;
			}
		} 
	return;
}

void Lib::init(string t, int max){
	maxSongs = max;
	//do time parsing
	stringstream ss(t);
	string h, m;
	int hi, mi,si;
	getline(ss, h, ':');
	getline(ss, m, ':');
	ss >> si;
	stringstream s2(h);
	stringstream s3(m);
	s2 >> hi;
	s3 >> mi;

	Time* nt = new Time(hi,mi,si);	
	currTime = nt;
	playlist = new Song[maxSongs];	
	hashtable = new Song*[maxSongs];
	for(int i =0; i < maxSongs; i++){
		hashtable[i] = NULL;
	}
}

void Lib::add(string title, string artist, string length){
	//parse to time
	stringstream ss(length);
  string  m;
  int  mi,si;
  getline(ss, m, ':');
  ss >> si;
  stringstream s3(m);
  s3 >> mi;	
	Time* nt = new Time(mi, si);

	Song ns;
	ns.title = title;
	ns.artist = artist;
	ns.runtime = nt;	
	Time* t2 = new Time(currTime->day, currTime->hour, currTime->min, currTime->sec);
	ns.timeadded = t2;
	ns.lastplayed = ns.timeadded;
	ns.update_key(currTime, ns.lastplayed);
	int i = 0;
	if(currSongs < maxSongs){
		currSongs++;
		playlist[currSongs-1] = ns;
	}

	insert_hashtable(&playlist[currSongs-1]);

//	cout << "added: " << ns.title << " by " << ns.artist << " key: "<< ns.key << endl;
	
	return;
}

void Lib::play(int n){
	if(n > 1000 && n < 1){
		cout << "cannot play that amount of songs" <<endl;
		return;
	}else{
		update_priorities();
		//for(int i = 0; i <= currSongs; i++){
		//	cout <<playlist[i].title << " " << playlist[i].key << endl;
		//	}
		cout <<"PLAY " << n << endl;
		for(int i = 0; i <= (n-1); i++){
			currTime->printTimestamp();
	//		cout << " key: " <<playlist[0].key << " ";
			playlist[0].printSong();
			currTime->add_time(playlist[0].runtime);
			Time* t3 = new Time(currTime->day, currTime->hour, currTime->min, currTime->sec);
			playlist[0].lastplayed = t3; 
			song_currently_playing = playlist[0];
      playlist[0].update_key(currTime, playlist[0].lastplayed);
			update_priorities();
		//	buildmaxheap();	
		}
	}
}

void Lib::rest(string r){
	stringstream ss(r);
  string h, m;
  int hi, mi,si;
  getline(ss, h, ':');
  getline(ss, m, ':');
  ss >> si;
  stringstream s2(h);
  stringstream s3(m);
  s2 >> hi;
  s3 >> mi;
  
	currTime->hour +=	hi;
	currTime->min += mi;
	currTime->sec += si;
}

void Lib::like(){
	if(song_currently_playing.title.empty()){
		Song* ns = search(playlist[currSongs-1].title);
    ns->like();
  //  cout << "likes for " << ns->title << " = " << ns->likes <<endl;
    update_priorities();
	}
	else{
		Song* ns = search(song_currently_playing.title);
		ns->like();
	//	cout << "likes for " << ns->title << " = " << ns->likes <<endl;
		update_priorities();
	} 
}

void Lib::dislike(){
	if(song_currently_playing.title.empty()){
    Song* ns = search(playlist[currSongs-1].title);
    ns->like();
//    cout << "likes for " << ns->title << " = " << ns->likes <<endl;
    update_priorities();
  }else{ 
		Song* nd = search(song_currently_playing.title);
		nd->dislike();
//		cout << "likes for " << nd->title << " = " << nd->likes <<endl;
		update_priorities();
	}
}

void Lib::like(string title){
	Song* ns = search(title);
	ns->like();
//	cout << "likes for " << title << " = " << ns->likes <<endl;
	update_priorities();
}

void Lib::dislike(string title){
	Song* nd = search(title);
	nd->dislike();
//	cout << "likes for " << title << " = " << nd->likes <<endl;
	update_priorities();
}

void Lib::quit(){
	return;
}

void Lib::print_playlist(){
	for(int i = 0; i <= (currSongs-1); i++){
		playlist[i].printSong();
		cout << playlist[i].key <<endl;
	}
}

//heap functions

void Lib::update_priorities(){
	for(int j = 0; j <= (currSongs-1); j++){
    playlist[j].update_key(currTime, playlist[j].lastplayed);
  }
	buildmaxheap();
}

int Lib::heap_key(Song& s){
	s.update_key(currTime, s.lastplayed);
	return s.key;
}

int Lib::getleft(int parent){
   int i = ( parent * 2 ) + 1; 
   return i;
}

int Lib::getright(int parent){
  	int i = ( parent * 2 ) + 2; 
    return i;
}

int Lib::getparent(int child){
    if (child != 0){
      int i = (child - 1) /2;
      return i;
    }
    return -1;
}

void Lib::swap(int i, int j){
   Song tmp; 
   tmp = playlist[i]; 
   playlist[i] = playlist[j];
   playlist[j] = tmp; 
   return; 
}

bool Lib::comp(int pnt,int pos){
	int key1 = heap_key(playlist[pnt]);
	int key2 = heap_key(playlist[pos]);
  if(key1 > key2){
		return true; 
  }
  else{
    return false;
  }
}

void Lib::maxheapify(int i){
	int largest;
	int left = getleft(i);
	int right = getright(i);
	if(i >= currSongs){
		return;	
	}
	if(left < currSongs && comp(left, i)){
			largest = left;
	}	
	else{
		largest = i; 
	}
	if(right < currSongs &&  comp(right, largest)){
		largest = right;
	}
	if(largest != i){
		swap(i, largest);
		maxheapify(largest);
	}
  return;
}

void Lib::buildmaxheap(){
	for(int i = floor((currSongs-1)/2); i >= 0 ; i--){
		maxheapify(i);
	}
	return;
}

//hash functions
int Lib::Hash(string key){
	int hash = 0;
	int index;
	for(int i = 0; i < key.length(); i++){
		hash += (int)key[i];
	}
	index = hash % maxSongs;
	return index;
}

void Lib::insert_hashtable(Song* sn){
	int index = Hash(sn->title);
	while(hashtable[index] != NULL){
		index = (index+1) % maxSongs; //type of chaining
	}
	hashtable[index] = sn;
}

Song* Lib::search(string key){
	int index = Hash(key);
	bool foundkey = false;	
	while(!foundkey){
		if(hashtable[index] == NULL){
			index = (index +1) % maxSongs;
		}else{
			if((hashtable[index]->title) == key){
				foundkey = true;
			}else{
				index = (index + 1) % maxSongs;
			}
		}
	}
	return hashtable[index];
}

